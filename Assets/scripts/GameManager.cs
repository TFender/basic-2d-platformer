﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {         

        private void Start() {
            //GetComponent<AudioSource>().Play();
            //SpawnStuff();
            StartCoroutine(GameLoop());
        }


        private IEnumerator GameLoop()
        {
            yield return StartCoroutine(RoundStarting());
            yield return StartCoroutine(RoundPlaying());
            yield return StartCoroutine(RoundEnding());
        }


        private IEnumerator RoundStarting()
        {   
			//dostuff
			return null;
        }


        private IEnumerator RoundPlaying()
        {
            //dostuff
			return null;
        }


        private IEnumerator RoundEnding()
        {
            //dostuff
			return null;
        }
			
        private IEnumerator waitAWhile()
        {
            yield return new WaitForSeconds(0.5f);
        }
    }
}