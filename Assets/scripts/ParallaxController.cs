﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

public class ParallaxController : MonoBehaviour
{
    public Layer[] layers;
	public Camera myCamera;
    public int zoomx;
    public int zoomy;
    public int zIndex;
    public float yModifier;

	void Start()
	{
	    foreach (var l in layers)
	    {
	        var quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
	        quad.transform.parent = transform;
	        quad.transform.position = new Vector3(0, 0, (layers.Length - l.layerOrder)+zIndex);
	        quad.transform.localScale = new Vector3(zoomx, zoomy);
	        quad.GetComponent<Renderer>().material = new Material(Shader.Find("Unlit/Transparent"));
	        quad.GetComponent<Renderer>().material.mainTexture = l.sprite;
	        quad.GetComponent<Renderer>().material.mainTexture.wrapMode = TextureWrapMode.Repeat;
            var camWidth = (Screen.width*1.7);
	        var textureWidth = quad.GetComponent<Renderer>().material.mainTexture.width;
	        float xratio = (float) camWidth / textureWidth;
            quad.transform.localScale = new Vector3(zoomx * xratio, zoomy);
	        quad.GetComponent<Renderer>().material.mainTextureScale = new Vector2(xratio, 1);
	        l.quad = quad;
	    }
	    transform.parent = myCamera.transform;
	}

	void Update()
	{
        Vector3 currCamPos = myCamera.transform.position;
		float xPos = currCamPos.x;
        float yPos = currCamPos.y;
	    transform.position = new Vector3(xPos, yPos / yModifier);

        foreach (var l in layers)
	    {
	        adjustParallaxPositionsForArray(l.quad.GetComponent<Renderer>(), l.modifier, l.scrollingspeed, xPos);
        }
    }

	void adjustParallaxPositionsForArray(Renderer sprite, float modifier, float scrollingspeed, float xPos)
	{
	    Vector2 offset = new Vector2();
	    if (scrollingspeed != 0)
	    {
	        offset = new Vector2(Time.time * (scrollingspeed/500), 0);
	    }
	    if (modifier != 0)
	    {
	        offset += new Vector2(xPos * (modifier/500), 0);
	    }
	    sprite.material.mainTextureOffset = offset;
	}
}
[Serializable]
public class Layer
{
    public Texture sprite;
    public float scrollingspeed;
    public float modifier;
    public int layerOrder;
    public GameObject quad;
}